"use strict"

module.exports = {
    Server: {
        name: 'Example Service',
        host: '0.0.0.0',
        port: '8081',
    },
    Authentication: {
        secret: 'sdf656sd4f5ss5gh89'
    },
    Features : {
        upload : false
    },
    Folders : {

    },
    Database: {
        test: {

        },
        dev: {

        },
        prod: {

        }
    },

}