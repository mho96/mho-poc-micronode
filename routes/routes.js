"use strict"

const Content = require('./content');

class Routes {
  constructor(server) {
    this.list = [];
    this.list.push(new Content(server));
  }
}

module.exports = Routes;