"use strict"
let Route = require('@fmguimaraes/micronode').Route;
class Content extends Route {
    constructor(server) {
        super(server);
        this.routes = [
            { path: "/contents/:id", get: this.getOne.bind(this) },
            { path: "/contents", get: this.getMany.bind(this) },
            { path: "/contents", post: this.create.bind(this) },
            { path: "/contents/:id", put: this.replace.bind(this) },
            { path: "/contents/:id", patch: this.update.bind(this) },
            { path: "/contents/:id", delete: this.remove.bind(this) },
        ];
    }

    getOne(req, res) {
        this.eventEmitter.emit('content.getOne', req, res);
    }

    getMany(req, res) {
        this.eventEmitter.emit('content.getMany', req, res);
    }

    create(req, res) {
        this.eventEmitter.emit('content.create', req, res);
    }

    replace(req, res) {
        this.eventEmitter.emit('content.replace', req, res);
    }

    update(req, res) {
        this.eventEmitter.emit('content.update', req, res);
    }

    remove(req, res) {
        this.eventEmitter.emit('content.remove', req, res);
    }

};

module.exports = Content;