
"use strict"
const Action = require('./action')
const contentService = require('./../services/ContentService');
const uniqid = require('uniqid');

class ContentActions extends Action {
    constructor(app) {
        super(app)
        this.eventEmitter.on('content.getOne', this.getOne.bind(this));
        this.eventEmitter.on('content.getMany', this.getMany.bind(this));
        this.eventEmitter.on('content.create', this.create.bind(this));
        this.eventEmitter.on('content.replace', this.replace.bind(this));
        this.eventEmitter.on('content.update', this.update.bind(this));
        this.eventEmitter.on('content.remove', this.remove.bind(this));
    }

    async getOne(req, res) {
        try {
            const result = await contentService.getOne(req.params.id);
            if (result === null) {
                throw new Error(`getOne(${req.params.id}) - redis returned null`);
            }
            res.status(200).send({ id: req.params.id, ...JSON.parse(result)});
        } catch (err) {
            console.log(err);
            res.status(500).send('Internal server error');
        }
    }
    
    async getMany(req, res) {
        try {
            const contents = await contentService.getMany();
            if (contents === null) {
                throw new Error(`getMany() - redis returned null`);
            }
            res.status(200).send(Object.keys(contents).map(val => {
                return {
                    id: val,
                    ...JSON.parse(contents[val])
                }
            }));
        } catch (err) {
            console.log(err);
            res.status(500).send('Internal server error');
        }
    }
    
    async create(req, res) {
        try {
            const id = uniqid();
            const fields = {
                title: req.body.title, 
                body: req.body.body
            };
            const payload = {
                ...fields,
                created: Math.floor(Date.now()/1000)
            };
    
            const result = await contentService.create(id, JSON.stringify(payload));
            if (!result) {
                throw new Error(`redis doesnt create the ressource for payload ${JSON.stringify(payload)}`);
            }
            await this.getOne({ params: { id } }, res);
        } catch (err) {
            console.log(err);
            res.status(500).send('Internal server error');
        }
    }
    
    async replace(req, res) {
        try {
            const fields = {
                title: req.body.title, 
                body: req.body.body
            };
            const created = Math.floor(Date.now()/1000);
            const payload = { ...fields, created: created };
            const result = await contentService.create(
                req.params.id,
                JSON.stringify(payload)
            );
    
            if (result === null) {
                throw new Error(`replace(${req.params.id}) - ${JSON.stringify(payload)} - redis returned null`);
            }
            await this.getOne(req, res);
        } catch (err) {
            console.log(err);
            res.status(500).send('Internal server error');
        }
    }
    
    async update(req, res) {
        try {
            const { id, ...existing } = await this.getOne(req, res);
            const existingKeys = Object.keys(existing);
            const fields = Object.keys(req.body).filter(
                key => !['id', 'created'].includes(key) && existingKeys.includes(key)
            ).reduce((acc, curr) => {
                return { ...acc, [curr]: req.body[curr] }
            }, {});
        
            const merge = { ...existing, ...fields };
            const created = Math.floor(Date.now()/1000);
            const payload = { ...merge, created: created };
    
            await contentService.create(
                id,
                JSON.stringify(payload)
            );
            await this.getOne(req, res);
        } catch (err) {
            console.log(err);
            res.status(500).send('Internal server error');
        }
    }
    
    async remove(req, res) {
        try {
            const result = await contentService.delete(req.params.id);
            if (!result) {
                throw new Error(`remove(${req.params.id}) - redis returned null`);
            }
            const message = result 
            ? `content ${req.params.id} has been deleted`
            : `content ${req.params.id} has not been deleted`
            res.status(200).send({ message });
        } catch (err) {
            console.log(err);
            res.status(500).send('Internal server error');
        }
    }

};

module.exports = ContentActions;