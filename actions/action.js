"use strict"
var M2Object = require('./../micronode/M2Object');

class Action extends M2Object {
    constructor(server) {
        super(server)
        this.server = server;
        this.eventEmitter = server.eventEmitter;
        this.socket = server.socket;
    }
}

module.exports = Action;