"use strict"

const ContentActions = require('./content');

class Actions {
  constructor(app) {
   this.content = new ContentActions(app);
  }
};

module.exports = Actions;